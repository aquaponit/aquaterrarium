import app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY ,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID
}

class Firebase {
    constructor() {
        app.initializeApp(firebaseConfig)
        this.firestore = app.firestore()
        this.auth = app.auth()
    }

    /** AUTH */
    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password)
 
    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password)
 
    doSignOut = () => this.auth.signOut()
 
    doPasswordReset = email => this.auth.sendPasswordResetEmail(email)
 
    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password)
    

    /** FIRESTORE */
    getCollection = collectionName =>
        this.firestore.collection(collectionName).get()

    getDocFromCollection = (collectionName, key) => 
        this.firestore.collection(collectionName).doc(key).get()
    
    createDocument = (collectionName, data) =>
        this.firestore.collection(collectionName).add(data)
    
    getTerriariumsFromUser = uid =>
        this.firestore.collection("user_data").doc(uid).collection("aquaterrarium")

    getTerrariumData = (uid, terrariumId) =>
        this.firestore.collection("user_data").doc(uid).collection("aquaterrarium").doc(terrariumId).collection("data").get()

    getTerrariumDataByDateRange = (uid, terrariumId, startDate, endDate) =>
        this.firestore.collection("user_data").doc(uid).collection("aquaterrarium").doc(terrariumId).collection("data").where('timestamp', '>=' , startDate).where('timestamp', '<=', endDate).orderBy('timestamp')
 
    createTerrarium = (uid, data) =>
        this.firestore.collection("user_data").doc(uid).collection("aquaterrarium").doc().set(data)
    /*TimeStamp */
    getTimeStamp =(date) =>
        app.firestore.Timestamp.fromDate(date)

    getServerTimestamp = () =>
        app.firestore.FieldValue.serverTimestamp()
}

export default Firebase