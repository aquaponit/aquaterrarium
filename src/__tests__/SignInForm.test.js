import React from 'react';
import ReactDOM from 'react-dom';
import SignInForm from '../Component/SignInForm';
import { BrowserRouter } from 'react-router-dom'

it('renders without crashing', () => {
    Object.defineProperty(window, 'matchMedia', {    
        value: jest.fn().mockImplementation(() => ({                              
          addListener: () => {}                         
        })),
    });

    const div = document.createElement('div');
    ReactDOM.render(
        <BrowserRouter>
            <SignInForm />
        </BrowserRouter>        
    , div);
  });