import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../../Component/Nav/Header';
import { BrowserRouter } from 'react-router-dom'
import Firebase, { FirebaseContext } from "../../Firebase/index";


it('renders without crashing', () => {
    // Mock
    Object.defineProperty(window, 'matchMedia', {    
        value: jest.fn().mockImplementation(query => ({
          matches: false,
          media: query,
          onchange: null,
          addListener: () => {}, 
          removeListener: () => {},
          addEventListener: () => {},
          removeEventListener: () => {},
          dispatchEvent: () => {},
        })),
    });
    
    const div = document.createElement('div');
    ReactDOM.render(
      <BrowserRouter>
        <FirebaseContext.Provider value={new Firebase()}>
            <Header />
        </FirebaseContext.Provider>
      </BrowserRouter>, div);
  });