import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from '../../Component/Dashboard/Dashboard';
import { BrowserRouter } from 'react-router-dom'
import Firebase, { FirebaseContext } from "../../Firebase/index";
import {Switch, Route} from 'react-router-dom';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <BrowserRouter>
        <FirebaseContext.Provider value={new Firebase()}>
          <Switch>
            <Route path="/dashboard" component={Dashboard}/>
          </Switch>        
        </FirebaseContext.Provider>
      </BrowserRouter>, div);
  });