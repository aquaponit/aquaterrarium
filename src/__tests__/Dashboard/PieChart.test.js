import React from 'react';
import ReactDOM from 'react-dom';
import PieChart from '../../Component/Dashboard/Charts/PieChart';

// Mock props
const props = {
    terrariumDataLength: 0,
    terrariumData: []
}

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PieChart {...props} />, div);
});