import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu } from 'antd'
import { withRouter } from 'react-router'
import { withFirebase } from '../../Firebase'

class RightMenu extends Component {
    static propTypes = {
        handleClick: PropTypes.func,
        firebase: PropTypes.object,
        current: PropTypes.string,
        history: PropTypes.any,
        authUser: PropTypes.object,
        style: PropTypes.object
    }
    
    logOut = () => {
        const { firebase, history } = this.props
        firebase.doSignOut().then(() => {
            history.push('/')            
        }).catch((error) => {
            console.error(error)
        })        
    }

    render() {
        const { handleClick, current, history, authUser, style } = this.props

        const NotAuthContent = () => (
            <Menu mode="horizontal" selectedKeys={[current]} onClick={handleClick} style={style}>                            
                <Menu.Item key="login" onClick={() => history.push("/login")}>
                    Connexion
                </Menu.Item>    
                <Menu.Item key="signin" onClick={() => history.push("/signin")}>
                    Inscription
                </Menu.Item>                  
            </Menu>
        )

        const AuthContent = () => (
            <Menu mode="horizontal" selectedKeys={[current]} onClick={handleClick} style={style}>                            
                <Menu.Item key="param" onClick={() => history.push("/user")}>
                    Paramètre
                </Menu.Item>    
                <Menu.Item key="logout" onClick={this.logOut}>
                    Déconnetion
                </Menu.Item>                  
            </Menu>
        )

        return (
            authUser ? <AuthContent/> : <NotAuthContent/>
        )
    }
}

export default withRouter(withFirebase(RightMenu))