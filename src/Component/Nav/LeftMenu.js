import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu } from 'antd'
import { withFirebase } from '../../Firebase'
import { withRouter } from 'react-router'

class LeftMenu extends Component {
    static propTypes = {
        handleClick: PropTypes.func,
        firebase: PropTypes.object,
        current: PropTypes.string,
        history: PropTypes.object,
        authUser: PropTypes.object,
        style: PropTypes.object
    }



    render() {
        const { handleClick, current, history, authUser, style } = this.props

        const AuthContent = () => (
            <Menu mode="horizontal" selectedKeys={[current]} onClick={handleClick} style={style}>                            
                <Menu.Item key="home" onClick={() => history.push("/")}>
                    Accueil
                </Menu.Item>
                <Menu.Item key="test" onClick={() => history.push("/dashboard")}>
                    Dashboard    
                </Menu.Item>                             
            </Menu>
        )

        const NotAuthContent = () => (
            <Menu mode="horizontal" selectedKeys={[current]} onClick={handleClick} style={style}>                            
                <Menu.Item key="home" onClick={() => history.push("/")}>
                Accueil
                </Menu.Item>                                        
            </Menu>
        )

 
        return (
            authUser ? <AuthContent/> : <NotAuthContent/>
        )
    }
}

export default withRouter(withFirebase(LeftMenu))
