import React , {Component} from 'react';
import { withRouter } from 'react-router-dom';
// import SignOutButton  from "./SignOutButton";
// import { Button, Row, Col, Menu } from 'antd'
import PropTypes from 'prop-types';
import RightMenu from "./RightMenu";
import LeftMenu from './LeftMenu';
import { Row, Col, Menu } from 'antd';

const menuStyle = {    
    fontWeight: "bold",
    backgroundColor: "#C4E1CC"
}
class Header extends Component{

    constructor(props) {
        super(props)
        this.state = {
            current: ''
        }
    }

    handleClick = (event) => {
        this.setState({ current: event.key });
    }

    render(){
        const { authUser } = this.props
        const { current } = this.state

        return (
            <div>
            <Row align="middle" justify="end">
                <Col span={4}>
                    <Menu mode="horizontal" selectable={false} style={menuStyle}>
                        <Menu.Item><img src="./Logo.svg" height="44px" width="31px"></img></Menu.Item>
                    </Menu>
                </Col>
                <Col span={14}>
                    <LeftMenu handleClick={this.handleClick} current={current} authUser={authUser} style={menuStyle}/>
                </Col>
                <Col span={6}>
                    <RightMenu handleClick={this.handleClick} current={current} authUser={authUser} style={menuStyle}/>
                </Col>
            </Row>            
            </div>
        )
    }
}

Header.propTypes = {
    authUser: PropTypes.object,
    history: PropTypes.object
}

export default withRouter(Header);