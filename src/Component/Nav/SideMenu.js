import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu, Row, Col } from 'antd'
import { SwapOutlined, PieChartOutlined, BarChartOutlined, LineChartOutlined, DashboardOutlined } from '@ant-design/icons'
import { withFirebase } from '../../Firebase'
import { withRouter } from 'react-router'

const iconStyle = {
    fontSize: 45,    
}

class SideMenu extends Component {
    static propTypes = {
        firebase: PropTypes.object,
        // history: PropTypes.object,
        authUser: PropTypes.object, changechart: PropTypes.func
    }

    componentDidMount = () => {
        // const { firebase, authUser } = this.props
        // firebase.getTerriariumsFromUser(authUser.uid).then(collection => {            
        //     if (collection.empty) {
        //         alert("Pas d'aquaterrarium crée")
        //     }
        //     collection.forEach(doc => {
        //         console.log(doc.data());
        //     })
        // })
    }

    render() {
        const {
            changechart
        } = this.props
        return (
            <Row justify={"center"}>
                <Col span={2}>
                    <LineChartOutlined style={iconStyle} onClick={() => changechart('line')} />
                </Col>
                <Col span={2}>
                    <PieChartOutlined style={iconStyle} onClick={() => changechart('pie')} />
                </Col>
                <Col span={2}>
                    <DashboardOutlined style={iconStyle} onClick={() => changechart('temp')} />
                </Col>
            </Row>
        )
    }
}

export default withRouter(withFirebase(SideMenu))
