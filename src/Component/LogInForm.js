import React, { Component } from 'react'
import { withFirebase } from '../Firebase'
import { withRouter } from 'react-router-dom'
import { Row, Input, Button, Form, Typography } from 'antd'
import PropTypes from 'prop-types';

export class LoginForm extends Component {    
    onFinish = (values) => {
        const { email, password } = values;
        // Log the user with form values
        this.props.firebase.doSignInWithEmailAndPassword(email, password)
        .then(user => {
            console.log(user)
            this.props.history.push("/dashboard")
        })
        .catch(error => {
            // Error triggered by an invalid mail or password
            console.error(`Error: ${error.code}  ${error.message}`)
        })
    }

    onFinishFailed = (errorInfo) => {
        console.log('Fail', errorInfo)
    }

    render() {
        const { Title } = Typography
        return (
            <div>
                <Title>Connexion</Title>
                <Row justify='center'>
                    <Form
                        onFinish={this.onFinish}
                        onFinishFailed={this.onFinishFailed}
                    >
                        <Form.Item
                            label="email"
                            name="email"
                            rules={[{type: 'email', message: 'Email non valide!'}, { required: true, message: 'Email requis!' }]}
                        >
                            <Input placeholder="email" name="email"></Input>
                        </Form.Item>

                        <Form.Item
                            label='Mot de passe'
                            name='password'
                            rules={[{ required: true, message: 'Mot de passe requis!' }]}
                        >
                            <Input.Password placeholder='password' name='password'/>
                        </Form.Item>

                        <Form.Item>
                            <Button type='primary' htmlType='submit'>Se connecter</Button>
                        </Form.Item>

                    </Form>
                </Row>
            </div>
        )
    }
}

LoginForm.propTypes = {
    firebase: PropTypes.object,
    history: PropTypes.object
}

export default withRouter(withFirebase(LoginForm))
