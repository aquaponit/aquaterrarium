import React, { Component } from 'react'
import { Card, Row, Col, Typography } from 'antd'

import pic from '../img/Aquaponie1.png';

const cardStyle ={
    justyfy: 'center',
     background: 'linear-gradient(#b3ffcc,#b3e6ff)'
}
class Home extends Component {

    render() {
        return (
        
         
                <div className="site-card-wrapper" style={{ marginTop:"30px"}}>
<Row justify= "space-around">
<Col span={16}>
        <Card  bordered={true}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>QU`EST-CE QUE L`AQUAPONIE?</Typography.Title>
        <Typography.Text strong={true}><h3>L’aquaponie est une méthode efficace pour cultiver vos fruits et légumes organiques, frais et de saison chez vous sans utiliser d’engrais ni de pesticides.</h3></Typography.Text><br/>
        <img src={pic} alt="" height='500px' width=' 700px'/>
        </Card>
              
      </Col>
   </Row>
 <div style={{ marginTop:"30px"}}>
    <Row gutter={[16, 24]}  justify= "space-around"  >
      
      <Col span={5}  >
        <Card style={cardStyle}
          bordered={true}>
         <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Mangez bio</Typography.Title>
        Avec l’aquaponie vous pouvez cultiver vos légumes et élever vos poissons de façon biologique à domicile. Vous pouvez débuter l’aquaponie même si vous habitez en ville grâce aux kits aquaponiques d’intérieur. En plus du plaisir de cultiver sainement, vous vous refaites une santé en mangeant mieux.
        </Card>
      </Col>
      <Col span={5}>
        <Card bordered={true} style={cardStyle}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Economisez</Typography.Title>
        En Aquaponie vous économisez 95% d’eau par rapport à la culture en terre. La consommation électrique d’un système aquaponique est d’environ 1€ par mois, mais produire votre nourriture peut vous faire économiser quelques centaines d’euros d’achats alimentaires chaque mois.
        </Card>
      </Col>
     
      <Col span={5}>
        <Card bordered={true} style={cardStyle}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Un geste écocitoyen</Typography.Title>
        Faites votre part du colibri avec ce premier pas vers l’autoproduction familiale à domicile en zone urbaine et rurale. En produisant votre nourriture, vous améliorez votre capacité de résilience et de votre sécurité alimentaire.
        </Card>
      </Col>
        </Row>
</div>

    <Row justify= "space-around"  >

      <Col span={5}>
        <Card   bordered={true} style={cardStyle}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Culture biologique</Typography.Title>
        Même si le label bio n’existe pas encore en France pour la culture hors sol, vous savez qu’en nourrissant vos poissons avec de la nourriture bio, vos productions de légumes et de poissons seront biologiques.
        </Card>
      </Col>

      <Col span={5}>
        <Card  bordered={true} style={cardStyle}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Pédagogique</Typography.Title>
        Il n’y a pas plus ludique que l’aquaponie. Les adultes redeviendront des enfants pour le plus grand plaisir des leurs qui s’émerveilleront devant cet écosystème vivant qu’ils auront plaisir à alimenter et entretenir au quotidien.
        </Card>
      </Col>

      <Col span={5} >
        <Card bordered={true} style={cardStyle}>
        <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>L’agriculture du futur</Typography.Title>
        L’aquaponie se développe rapidement en France et de nombreux métiers risquent de voir le jour. C’est le moment de ne pas prendre un train de retard et de commencer à envisager votre nouvelle vie de cultivateur urbain!
        </Card>
      </Col>
      </Row>
    
  </div>

           
            )
    }
}

export default Home;