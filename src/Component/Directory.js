import React, { Component } from 'react';
import { getAllDevs } from '../FakeApiBilal';

class Directory extends Component {
    handleClickdevs = (id) =>{
        console.log(id);
    }

    render() {
        const devsList = getAllDevs();
        return (
            <div>
                <h2> Directory</h2>
                <div>
                    {devsList.map(devs => {
                        return (
                            <div onClick={this.handleClickdevs.bind(devs.id)} key={devs.id}> 
                                {devs.name}
                            </div>
                        )



                    })}
                </div>
            </div>
        )
    }
}
export default Directory;
