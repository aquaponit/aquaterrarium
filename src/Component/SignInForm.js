import React, { Component } from 'react'
import { Row, Input, Button, Form, Typography } from 'antd'
import { withFirebase } from '../Firebase'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';
export class SignInForm extends Component {

    onFinish = (values) => {
        const { email, password } = values
        // Create user account with form values
        this.props.firebase.doCreateUserWithEmailAndPassword(email, password)
        .then(authUser => {
            this.props.history.push("/dashboard")
            console.log("Success to create account", authUser)
        })
        .catch(error => {
            console.log("Error when creating account", error)
        })        
    }

    onFinishFailed = (errorInfo) => {
        console.log('Fail', errorInfo)
    }

    render() {
        const { Title } = Typography
        return (
            <div>
                <Title>Inscription</Title>
                <Row justify='center'>
                    <Form
                        onFinish={this.onFinish}
                        onFinishFailed={this.onFinishFailed}
                    >                        
                        <Form.Item
                            label='Email'
                            name='email'                        
                            rules={[{type: 'email', message: 'Email non valide!'}, { required: true, message: 'Email requis!' }]}
                        >
                            <Input placeholder='email' name='email'/>
                        </Form.Item>
                        
                        <Form.Item
                            label='Mot de passe'
                            name='password'
                            rules={[{ required: true, message: 'Mot de passe requis!' }]}
                        >
                            <Input.Password placeholder='password' name='password'/>
                        </Form.Item>

                        <Form.Item>
                            <Button type='primary' htmlType='submit'>Sinscrire</Button>
                        </Form.Item>
                    </Form>
                </Row>        
            </div>
        )
    }
}

SignInForm.propTypes = {
    firebase: PropTypes.object,
    history: PropTypes.object
}

export default withRouter(withFirebase(SignInForm))
