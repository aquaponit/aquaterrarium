import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Line } from '@ant-design/charts'
import moment from 'moment'
import 'moment/locale/fr'
moment.locale('fr')



class LineChart extends Component {
    static propTypes = {
        terrariumData: PropTypes.array,
        terrariumDataLength: PropTypes.number
    }


    getValue = () => {
      const { terrariumData } =this.props 
        let finalData = []
        terrariumData.forEach(element => {
          finalData.push({
            Time:moment(element.timestamp.toDate()).format('YYYY-MM-DD , h:mm:ss'),
            Value: element.potassium,
            name : 'potassium'
          })
          finalData.push({
            Time:moment(element.timestamp.toDate()).format('YYYY-MM-DD , h:mm:ss'),
            Value: element.phosphore,
            name : 'phosphore'

          })
          
        });
        return finalData
    }


    render() {
        
        const { terrariumDataLength } = this.props
        console.log(terrariumDataLength)
        const test = this.getValue()
            console.log(test)
        const config = {
          data: this.getValue(),
          xField: 'Time',
          yField: 'Value',
          seriesField: 'name',
          yAxis: {
            label: {
              formatter: function formatter(v) {
                return ''.concat((v / 1000000000).toFixed(1), ' B');
              },
            },
          },
          legend: { position: 'top' },
          smooth: true,
          animation: {
            appear: {
              animation: 'path-in',
              duration: 5000,
            },
          },
        };

        return (
            <div>
                <Line {...config}/>
            </div>
        )
    }
}

export default LineChart
