import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Pie } from '@ant-design/charts'



class PieChart extends Component {
    static propTypes = {
        terrariumData: PropTypes.array,
        terrariumDataLength: PropTypes.number
    }

    constructor(props) {
        super(props)
        this.state = {
            sliderValue: ''
        }
    }
    sliderChange = (value) => {
        console.log(value)
    }

    render() {
        
        const { terrariumData } = this.props
        const lastTerrarium = terrariumData[terrariumData.length - 1]
        console.log(lastTerrarium)
        
        const data = terrariumData.length ? 
            [
                {
                    type: "Ammoniaque",
                    value: lastTerrarium.ammoniaque
                },
                {
                    type: "Phosphore",
                    value: lastTerrarium.phosphore
                },
                {
                    type: "Potassium",
                    value: lastTerrarium.potassium
                },
            ] : []
        const config = {
            appendPadding: 10,
            data: data,
            angleField: 'value',
            colorField: 'type',
            radius: 0.8,
            label: {
              type: 'outer',
              content: '{name} {percentage}',
            },
            interactions: [{ type: 'pie-legend-active' }, { type: 'element-active' }],
        }

        return (
            <div>
                <Pie {...config}/>
            </div>
        )
    }
}

export default PieChart
