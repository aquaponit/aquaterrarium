import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Gauge, Bullet } from '@ant-design/charts'
import { Row, Col } from 'antd'




class TempChart extends Component {
    static propTypes = {
        terrariumData: PropTypes.array,
        terrariumDataLength: PropTypes.number
    }

    constructor(props) {
        super(props)
        this.state = {
            sliderValue: ''
        }
    }
    sliderChange = (value) => {
        console.log(value)
    }

    render() {
        
        const { terrariumData } = this.props
        const lastTerrarium = terrariumData[terrariumData.length - 1]
        console.log(lastTerrarium)
        
        const data = terrariumData.length ? 
        lastTerrarium.humidity   
        : 0
            const config = {
                percent: data/1000 ,
                range: { color: '#30BF78' },
                indicator: {
                  pointer: { style: { stroke: '#D0D0D0' } },
                  pin: { style: { stroke: '#D0D0D0' } },
                },
                axis: {
                  label: {
                    formatter: function formatter(v) {
                      return Number(v) * 100;
                    },
                  },
                  subTickLine: { count: 3 },
                },
                statistic: {
                  content: {
                    formatter: function formatter(_ref) {
                      var percent = _ref.percent;
                      return 'Rate: '.concat((percent * 100).toFixed(0), '%');
                    },
                    style: {
                      color: 'rgba(0,0,0,0.65)',
                      fontSize: 48,
                    },
                  },
                },
            }

            const bulletData = terrariumData.length ? 
            [{
                title: 'Température',
                ranges: [50],
                measures: [lastTerrarium.tempsReelCelsuis],
                target: 50,
            }] :
            [{
              title: 'Température',
              ranges: [50],
              measures: [0],
              target: 50,
            }] 
        
            var bulletConfig = {
              data: bulletData,
              measureField: 'measures',
              rangeField: 'ranges',
              targetField: 'target',
              xField: 'title',
              color: {
                range: '#f0efff',
                measure: '#5B8FF9',
                target: '#3D76DD',
              },
              xAxis: { line: null },
              yAxis: false,
              layout: 'vertical',
              label: {
                measure: {
                  position: 'middle',
                  style: { fill: '#fff' },
                },
              },
              bulletStyle: {
                style: {
                  lineWidth: 10
                }           
              }          
            };

        return (
            <Row align="middle" justify="center">
              <Col>
                <Gauge {...config}/>
              </Col>                
              <Col>
                <Bullet {...bulletConfig}/>
              </Col>                
            </Row>
        )
    }
}

export default TempChart
