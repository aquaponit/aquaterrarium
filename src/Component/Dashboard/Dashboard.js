import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Select, Typography, Row, Col, DatePicker, Carousel
} from 'antd';
import { withFirebase } from '../../Firebase';
import SideMenu from '../Nav/SideMenu';
import PieChart from './Charts/PieChart';
import locale from 'antd/es/date-picker/locale/fr_FR'
import moment from 'moment';
import LineChart from '../Dashboard/Charts/LineChart';
import TempChart from '../Dashboard/Charts/TempChart';
moment.locale('fr')

const cardStyle = {
  boxShadow: "5px 8px 24px 5px #c3d9cd",
  height: "80vh",
  marginBottom: "2vh",
  overflow: "scroll"
}
class Dashboard extends Component {

  static propTypes = {
    authUser: PropTypes.object,
    firebase: PropTypes.object,
  }
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment().subtract(1, "days"),
      endDate: moment(),      
      terrariumsInfo: [],
      terrariumData: [],
      user: {},
      selectedId: '',
      display: {
        pie: true,
        line: false,
        temp: false,
      }

    };
  }

  componentDidMount() {
    const { firebase } = this.props;
    // Get user info
    firebase.auth.onAuthStateChanged((user) => {
      if (user != null) {
        this.setState({
          user,
        });
        this.getTerrariums(user.uid);
      }
    });
  }
  changechart = (test) => {
    console.log("test55", test)
    switch (test) {
      case 'pie':
        this.setState({
          display: {
            pie: true,
            line: false,
            temp: false
          }
        })
        break 
  
      case'line':
        this.setState({
          display: {
            pie: false,
            line: true,
            temp: false
          }
        })
        break

      case'temp':
        this.setState({
          display: {
            pie: false,
            line: false,
            temp: true,
          }
        })
        break
    }
  }
     
  // Get all terrariums for user
  getTerrariums = (uid) => {
    const { firebase } = this.props;
    firebase.getTerriariumsFromUser(uid).get().then((collection) => {
      if (collection.empty) {
        alert("Pas d'aquaterrarium crée");
      }
      collection.forEach((doc) => {
        this.setState((prevState) => ({
          terrariumsInfo: [...prevState.terrariumsInfo, doc],
        }));
      });
      const { terrariumsInfo } = this.state;
      if (terrariumsInfo.length) {
        this.setState({
          selectedId: terrariumsInfo[0].id,
        }, () => { this.getDataFromTerrarium(); });
      }
    });
  }

  // getDataFromTerrarium = () => {
  //     const { user, selectedId } = this.state
  //     const { firebase } = this.props
  //     firebase.getTerrariumData(user.uid, selectedId).then(collection => {
  //         collection.forEach(doc => {
  //             this.setState(prevState => ({
  //                 terrariumData: [...prevState.terrariumData, doc.data()]
  //             }))
  //         })
  //     })
  // }

  // getDataFromTerrarium = () => {
  //   const { user, selectedId, startDate, endDate } = this.state;
  //   const { firebase } = this.props;      
  //   firebase.getTerrariumDataByDateRange(user.uid, selectedId, startDate.toDate(), endDate.toDate()).then((collection) => {
  //     let total = []
  //     collection.forEach((doc) => {
  //       console.log(doc.data())
  //       total.push(doc.data())
  //     });
  //     this.setState((prevState) => ({
  //       terrariumData: total,
  //     }));
  //   });
  // }

  getDataFromTerrarium = () => {
    const { user, selectedId, startDate, endDate } = this.state;
    const { firebase } = this.props;
    firebase.getTerrariumDataByDateRange(user.uid, selectedId, startDate.toDate(), endDate.toDate()).onSnapshot((collection) => {
      let total = []
      collection.forEach((doc) => {
        total.push(doc.data())
      });
      this.setState((prevState) => ({
        terrariumData: total,
      }));
    });
  }

  handleChange = (value) => {
    this.setState({
      selectedId: value,
      terrariumData: [],
    }, () => { this.getDataFromTerrarium(); });
    console.log(value)
  }

  handleDateRangeChange = (values) => {
    this.setState({
      startDate: values[0],
      endDate: values[1]
    }, () => { this.getDataFromTerrarium() })

  }

  handleDatePickerChange = (value) => {
    const { startDate } = this.setState
    if(moment(startDate).isAfter(value)) {
      this.setState({
        endDate: value,
        startDate: moment(value).subtract(1, 'day')
      }, () => { this.getDataFromTerrarium() })  
    } else {
      this.setState({
        endDate: value
      }, () => { this.getDataFromTerrarium() })
    }
    
  }

  render() {
    const { terrariumsInfo, terrariumData, user, startDate, endDate, selectedId } = this.state;
    const { RangePicker } = DatePicker

    const ChartSettings = ({terrariumsInfo}) => (
      <Col>
        <Row style={{marginTop: "10vh"}}>                            
          <Col span={24}>
          <Row justify="center">
            <Typography.Text strong={true}>Aquaterrarium:</Typography.Text>
          </Row>
          <Row justify="center">
            <Select style={{ width: 120 }} onChange={this.handleChange} defaultValue={terrariumsInfo[0].id} value={selectedId}>
              {terrariumsInfo.map((terrarium) => <Select.Option key={terrarium.id} value={terrarium.id}>{terrarium.data().name}</Select.Option>)}
            </Select>
          </Row>                        
          </Col>          
        </Row>
        <Row style={{marginTop: "10vh"}}>
          <Col span={24}>            
            {<Date />}
          </Col>            
        </Row>
      </Col>
    )

    const Date = () => {
      const { display, startDate, endDate } = this.state
      if(display.pie || display.temp)
        return (
          <div>
            <Row justify="center">
              <Typography.Text strong={true}>Selectionner une date:</Typography.Text>
            </Row>            
            <Row justify="center">
              <DatePicker showTime locale={locale} onChange={this.handleDatePickerChange} disabledDate={(current) => { return current > moment() }} format="DD/MM/YYYY HH:mm:ss" value={endDate} />
            </Row>            
          </div>        
        )
      else 
        return ( 
          <div>
            <Row justify="center">
              <Typography.Text strong={true}>Selectionner un intervalle de dates:</Typography.Text>
            </Row>            
            <Row justify="center">
              <RangePicker showTime locale={locale} onChange={this.handleDateRangeChange} disabledDate={(current) => { return current > moment() }} format="DD/MM/YYYY HH:mm:ss" value={[startDate, endDate]}/>
            </Row>            
          </div>
        )
    }

    const Chart = () => {
      if (this.state.display.pie)
        return (<PieChart terrariumData={terrariumData} terrariumDataLength={terrariumData.length ? terrariumData.length : 0} />)
      else if (this.state.display.line)
        return (<LineChart terrariumData={terrariumData} terrariumDataLength={terrariumData.length} />)
      else if (this.state.display.temp)
        return (<TempChart terrariumData={terrariumData} terrariumDataLength={terrariumData.length ? terrariumData.length : 0} />)
    }

    return (
      <div>
        <Row justify="space-between" style={{ marginTop: "4vh", marginLeft: "4vh", marginRight: "4vh" }}>
          <Col span={8} style={cardStyle}>
            <Row justify="center" style={{height: "10%"}}>
              <Typography.Title style={{ color: "#345242" , textAlign: "center"}}>Options</Typography.Title>
            </Row>
            {terrariumsInfo.length
              ? (
                <ChartSettings terrariumsInfo={terrariumsInfo} />
              ) : null}
          </Col>

          <Col span={15} style={cardStyle}>
            <Row justify="center">
              <Typography.Title style={{ color: "#345242", textAlign: "center", height:"10%" }}>Graphiques</Typography.Title>
            </Row>
            <Row align="middle" style={{height: "89%"}}>
              <Col span={24}>
                {<Chart />}
              </Col>            
            </Row>              
          </Col>    
        </Row>      
        <SideMenu authUser={user} changechart={this.changechart} />    
      </div>
    );
  }
}

export default withFirebase(Dashboard);
