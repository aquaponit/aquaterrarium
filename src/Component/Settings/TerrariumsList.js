import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'
import { withFirebase } from '../../Firebase'
import moment from 'moment'
import 'moment/locale/fr'
moment.locale('fr')

export class TerrariumsList extends Component {
    static propTypes = {
        firebase: PropTypes.object,
        userId: PropTypes.string
    }

    constructor(props){
        super(props)
        this.state = {
            terrarriumsInfo: []
        }
    }

    componentDidUpdate(prevProps) {
        // Do not update component if props are not changed
        if(this.props.userId === prevProps.userId) return        
        const { firebase, userId } = this.props
        // Get All terrariums info for one user REALTIME
        firebase.getTerriariumsFromUser(userId).onSnapshot(querySnapshot => {
            let result = []
            querySnapshot.forEach(doc => {   
                //Format data for display format
                let data = doc.data()
                data.id = doc.id                
                data.creation_time = moment(data.creation_time).format('MMMM Do YYYY')
                data.last_update = moment(data.last_update).format('MMMM Do YYYY, h:mm:ss')
                result = [...result, data]
            })
            this.setState({                    
                terrarriumsInfo: result
            }) 
        })
    }

    render() {
        const { terrarriumsInfo } = this.state
        const columns = [
            {
                title: 'Id',
                dataIndex: 'id',
                key: 'id'
            },
            {
                title: 'Nom',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'Model',
                dataIndex: 'model',
                key: 'model'
            },           
            {
                title: 'Création',
                dataIndex: 'creation_time',
                key: 'creation_time'
            },
            {
                title: 'Modification',
                dataIndex: 'last_update',
                key: 'last_update'
            }                        
        ]
        console.log(terrarriumsInfo)

        return (
            <Table dataSource={terrarriumsInfo} columns={columns} />
        )
    }
}

export default withFirebase(TerrariumsList)
