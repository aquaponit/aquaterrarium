import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withFirebase } from "../../Firebase";
import TerrariumForm from './TerrariumForm'
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import TerrariumsList from './TerrariumsList';

class Profil extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            userId: '',
            email: '',
            firstname: '',
            lastname: ''            
        };
    }

    componentDidMount() {
        const { firebase } = this.props
        // Get logged user data
        firebase.auth.onAuthStateChanged(user => {
            if (user != null) {
                const { uid, email } = user                
                this.setState({
                    userId: uid,
                    email: email
                })
                this.getUserData(uid)
            }
        })
    }

    getUserData = (uid) => {
        const { firebase } = this.props
        // Get user information via userId and store it as state
        firebase.getDocFromCollection('user_info', uid).then(doc => {
            if (doc.exists) {
                const data = doc.data()
                this.setState({
                    firstname: data.first_name,
                    lastname: data.last_name
                })
            } else {
                console.error(`No data for userId: ${uid}`)
            }            
        })
    }    

    render() {
        const { userId } = this.state        
        return (
            <Row align="middle" justify="center" style={{marginTop: '10vh', marginRight: '5vh', marginLeft: '5vh'}}>
                <Col span={12}>
                    <h5>Name: {this.state.firstname}</h5>
                    <h5>Role: {this.state.lastname}</h5>
                    <h5>Id: {this.state.userId}</h5>
                    <h5>Email: {this.state.email}</h5>
                </Col>
                <Col span={12}>
                    <TerrariumForm userId={userId} />
                    <TerrariumsList userId={userId} />
                </Col>
            </Row>
        )
    }
}

Profil.propTypes = {
    firebase: PropTypes.object
}

export default withRouter(withFirebase(Profil));