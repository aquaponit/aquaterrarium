import React, { Component } from 'react'
import { Row, Input, Button, Form } from 'antd'
import { withFirebase } from '../../Firebase'
import PropTypes from 'prop-types'

// var fire = require('firebase/app')

class TerrariumForm extends Component {
    static propTypes = {
        userId: PropTypes.string,
        firebase: PropTypes.object
    }
    constructor(props) {
        super(props)
        this.state = {
            terrarriumsInfo: []
        }
    }

    onFinish = (values) => {
        const { firebase, userId } = this.props
        // Format data from form
        const data = {
            creation_time: firebase.getServerTimestamp(),
            last_update: firebase.getServerTimestamp(),
            model: values.model,
            name: values.name,            
        }
        firebase.createTerrarium(userId, data)
    }

    onFinishFailed = (errorInfo) => {
        console.log('Fail', errorInfo)
    }
    
    

    render() {
        console.log(this.state.terrarriumsInfo)
        return (
            <Row justify='center'>
                <Form
                    onFinish={this.onFinish}
                    onFinishFailed={this.onFinishFailed}
                >
                    <Form.Item
                        label='Nom'
                        name='name'                        
                        rules={[{ required: true, message: 'Nom requis!' }]}
                    >
                        <Input placeholder='nom' name='name' />
                    </Form.Item>

                    <Form.Item
                        label='Model'
                        name='model'                        
                        rules={[{ required: true, message: 'Model requis!' }]}
                    >
                        <Input placeholder='model' name='model' />
                    </Form.Item>                                                              

                    <Form.Item>
                        <Button type='primary' htmlType='submit'>Créer</Button>
                    </Form.Item>
                </Form>
            </Row>            
        )
    }
}

export default withFirebase(TerrariumForm)
