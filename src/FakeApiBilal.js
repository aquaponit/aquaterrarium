const devs =[
    {
        id: "1",
        name:"bilal",
        role :"developpeur Front"
    },
    {
        id: "2",
        name:"Thomas",
        role :"developpeur Front"
    },
    {
        id: "3",
        name:"Latamen",
        role :"developpeur Back"
    },
    {
        id: "4",
        name:"Lucas",
        role :"developpeur FullStack"
    },
    {
        id: "5",
        name:"Latamen",
        role :"developpeur FullStack"
    },

];

export const getAllDevs = () =>{
    return devs;
};

export const getDevsById = (id) => {
    return devs.filter(devs =>{
        return devs.id === id;

    });

};