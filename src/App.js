import React, { Component } from 'react'
import './App.css';
//import {BrowserRouter,Route} from 'react-dom';
import Header from './Component/Nav/Header';
import Home from './Component/Home';
import Directory from './Component/Directory';
import {Switch, Route} from 'react-router-dom';
import Settings from './Component/Settings/Settings';
import SignInForm from './Component/SignInForm';
import LoginForm from './Component/LogInForm'
import Dashboard from "./Component/Dashboard/Dashboard";
import 'antd/dist/antd.css'
import '../src/Main.css'
import { withFirebase } from "./Firebase";
import PropTypes from 'prop-types';
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      authUser: null,
    }
  }

  componentDidMount() {
    const { firebase } = this.props
    // Get logged user information
    this.lister = firebase.auth.onAuthStateChanged(authUser => {
      authUser
        ? this.setState({ authUser })
        : this.setState({ authUser: null })
    })
  }

  componentWillUnmount() {
    this.lister()
  }
 

  render() {
    const { authUser } = this.state
    return (
      <div className="App">
        <Header authUser={authUser} />
        <Switch>
          <Route exact path="/" component={Home}/>          
          <Route path="/signin" component={SignInForm}/>
          <Route path="/login" component={LoginForm} />
          <Route path="/dashboard" component={Dashboard}/>

          <Route exact path="/directory" component={Directory}/>
          <Route path="/user" component={Settings}/>
        </Switch>
      </div>)
  }
}

App.propTypes = {
  firebase: PropTypes.object
}

export default withFirebase(App);
